/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Friend implements Serializable{
     private int id;
     private String name;
     private int age;
     private String tel;  
     private static int lastid =1;

    public Friend(String name, int age, String tel) {
        this.id = lastid++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }

    public static int getLastid() {
        return lastid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age)throws Exception{
        if(age<0){
        throw new Exception();
                
                }
        this.age = age;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public static void setLastid(int lastid) {
        Friend.lastid = lastid;
    }

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
     public static void main(String[] args) {
        Friend f = new Friend("Piggy", 500, "085124615");
         try {
             f.setAge(-1);
         } catch (Exception ex) {
             Logger.getLogger(Friend.class.getName()).log(Level.SEVERE, null, ex);
         }
         System.out.println(""+f);
    }
}
